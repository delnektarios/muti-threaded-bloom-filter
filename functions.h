
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#ifndef FUNCTIONS //include guard

#define FUNCTIONS

#define NUM 3;
#define MAX_SIZE 140000; //Bytes for bloom filter
#define perror2(s, e) fprintf(stderr, "%s: %s\n", s, strerror(e))


typedef struct arguments{
        long long size;
	int num_thr;
        int L;
        int port;
        int NUMBER_OF_LOCKS;
        int num;
	long long N;
        int seed;
        int socket;
        int file;
        char *bloom;
        pthread_mutex_t *mutex_bloom;
}arguments;


char *found;

pthread_mutex_t *mutex_bloom;

pthread_mutex_t let_server_copy;

void ask_oracle(int number_of_bits,char *ptr,int num,const char *word_for_oracle,int *flag,pthread_mutex_t *m,int n,int existing_depth,double *counter1,double *counter2);

int bloom_filter(int number_of_bits,uint64_t value_from_hash_by[],int num,unsigned char* word,pthread_mutex_t *m,int n);


#endif
