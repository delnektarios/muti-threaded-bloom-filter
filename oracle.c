
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <pthread.h> 
#include <sys/types.h>     // For sockets 
#include <sys/socket.h>    // For sockets 
#include <netinet/in.h>    // For Internet sockets 
#include <netdb.h> 
#include <time.h>
#include "oracle.h"
#include "functions.h"
#include "routines.h"

//#define CLIENT  //IF DEFINED THE PROGRAMM WILL WORK AS A SERVER

int main(int argc,char *argv[]){
        system("clear"); //cleaning screen
        srand(time(NULL));        
        int num=3,L,port,client_port;         //default value for number of hush functions
        int i=0,check=0,depth=10,err;
        long long size;
        int NUMBER_OF_LOCKS=0;
        char *bloom,*log=NULL,*addr=NULL;
        int n;
        int seed=rand();                

        if(argc>1){
                size=atoll(argv[1]);    //SIZE OF BLOOM FILTER IN BYTES
                n=atoi(argv[2]);        //NUMBER OF THREADS
                L=atoi(argv[3]);        //NUMBER OF REPEATS
                port=atoi(argv[4]);     //GETING PORT FOR SERVER TO LISTEN FOR REQUESTS
                log=malloc(strlen(argv[5])+1);
                memset(log,"\0",strlen(argv[5])+1);
                strcpy(log,argv[5]);    //LOGFILE NAME
                num=atoi(argv[6]);      //NUMBER OF HUSH FUNCTIONS
                if(argc>7){
                        addr=malloc(strlen(argv[7])+1); //SERVER ADDRESS FOR CLIENT TO REQUEST BLOOM FILTER
                        memset(addr,"\0",strlen(argv[7])+1);
                        strcpy(addr,argv[7]);
                        client_port=atoi(argv[8]);      //PORT SO CLIENT CAN SEND THE REQUEST TO SERVER
                }       
        }
//-------------------------------------------------------------------------------
//CODE TAKEN FROM CLASS
        #ifdef CLIENT
                uint64_t tmp;
                int sock; 
                 
                unsigned int serverlen,length=0,tmp1;
                struct sockaddr_in server;
                struct sockaddr *serverptr;
                struct hostent *rem=NULL;
                arguments b;

                if ((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0) { // Create socket 
                        perror("socket"); exit(1); }
                printf("sock to communicate is = %d\n",sock);
                if ((rem = gethostbyname(addr)) == NULL) { // Find server address 
                        herror("gethostbyname"); 
                        exit(1); 
                }
                printf("done gethostbyname\n");
                server.sin_family = PF_INET;                      // Internet domain 
                printf("%p\n",rem);
                //bcopy((char *) rem -> h_addr, (char *) &server.sin_addr,rem -> h_length);
                memcpy(&server.sin_addr,rem->h_addr,rem->h_length);
                printf("done memcpy\n");
                //server.sin_addr.s_addr = inet_addr("127.0.0.1");
                server.sin_port = htons(client_port); // Server's Internet address and port 
                serverptr = (struct sockaddr *) &server;
                serverlen = sizeof server;
                if (connect(sock, serverptr, serverlen) < 0) { // Request connection 
                        perror("connect"); exit(1); }
                printf("Requested connection to host %s port %d\n", addr, client_port);
                 
                //CLIENT READS ALL ARGUMENTS SENT FROM SERVER
                read_all(sock,&tmp,(uint64_t) sizeof(b.size));
                b.size=ntohl(tmp);
                printf("%lld\n",b.size);
                
                read_all(sock,&tmp1,(uint64_t) sizeof(b.num_thr));                
                b.num_thr=ntohl(tmp1);
                printf("%d\n",b.num_thr);
                
                read_all(sock,&tmp1,(uint64_t) sizeof(b.L));                
                b.L=ntohl(tmp1);
                printf("%d\n",b.L);
                
                read_all(sock,&tmp1,(uint64_t) sizeof(b.port));
                b.port=ntohl(tmp1);
                printf("%d\n",b.port);
                
                read_all(sock,&tmp1,(uint64_t) sizeof(b.num));
                b.num=ntohl(tmp1);
		printf("%d\n",b.num);

                read_all(sock,&tmp1,(uint64_t) sizeof(seed));
                seed=ntohl(tmp1);
                printf("%d\n",seed);

                printf("---------------------------------------------------\n");
                printf("%lld %d %d %d %d and seed %d\n",b.size,b.num_thr,b.L,b.port,b.num,seed);
                printf("---------------------------------------------------\n");
                
                //CHECKING ARGUMENTS TO DECIDE WHETHER TO REQUEST BLOOM FILTER OR NOT
                int send=0;
                if(num==b.num && size==b.size){
                        send=1; //INFORMING SEVER TO SEND BLOOM FILTER
                        write_all(sock,&send,sizeof(send));
                }
                else{
                        printf("INVALID ARGUMENTS\N");
                        exit(325);
                }
                
                bloom=malloc(size);
                for (i=0; i<size; i++) bloom[i]=0; //initializing bloom filter array of bits
                
                read_all(sock,bloom,size);      //DOWNLOADING BLOOM FILTER
                
                close(sock);
        #endif 
//--------------------------------------------------------------------       
        
        #ifndef CLIENT 
        	// get memory for the bloom filter and you get size*CHAR_BIT bits 
        	bloom=(unsigned char *)malloc(size*sizeof(char));
        	if(bloom==NULL) exit(2); //EXIT CODE 2 SUGGESTS MALLOC FAILED
        	for (i=0; i<size; i++) bloom[i]=0; //initializing bloom filter array of bits
        #endif 
             
        initAlloc(malloc);
        //setEasyMode();
        setHardMode();
        
        initSeed(seed); //initializing oracle

        if (n > 50) { //AVOIDING TO MANY THREADS
                fprintf(stderr,"Number of threads should be up to 50\n"); 
                exit(0); 
        }

        NUMBER_OF_LOCKS=(size/64)+1;    //CALCULATING NUMBER OF LOCKS NEEDED FOR EACH SECTION OF BLOOM FILTER
        mutex_bloom=malloc(NUMBER_OF_LOCKS*sizeof(pthread_mutex_t));

        for (i=0;i<NUMBER_OF_LOCKS;i++)
                pthread_mutex_init(&mutex_bloom[i],NULL);

        pthread_mutex_init(&let_server_copy,NULL);

        //CREATING THREADS
        pthread_t *thread_ids,server_thread_id;
        arguments arg;

        if((thread_ids=malloc(n*sizeof(pthread_t)))==NULL){
                perror("malloc"); 
                exit(100); 
        }

        //ALL ARGUMENTS OF THE PROGRAMM ARE "PACKED" IN A STRUCT
        arg.size=size;
        arg.num_thr=n;
        arg.L=L;   
        arg.port=port;       
        arg.NUMBER_OF_LOCKS=NUMBER_OF_LOCKS;       
        arg.num=num;      
        arg.N=CHAR_BIT*size;      
        arg.bloom=bloom;     
        arg.mutex_bloom=mutex_bloom;
        arg.seed=seed;
        arg.file=open(log,O_WRONLY|O_CREAT,0700);
        perror2("file",arg.file);
        printf("succesfully opened file  %d\n",arg.file);
        
        for(i=0;i<n;i++){
   	        if (err=pthread_create(&thread_ids[i],NULL,routine,&arg)){
      	                /* Create a thread */
      	                perror2("pthread_create",err); 
      	                exit(120); 
      	        } 
        }
        //creating thread for server
        if (err=pthread_create(&server_thread_id,NULL,server_thread,&arg)){
      	        /* Create a thread */
      	        perror2("pthread_create",err); 
      	        exit(122);
        }

        //joining all threads
        for (i=0 ; i<n ; i++){
                if(err=pthread_join(thread_ids[i],NULL)){
                /* Wait for thread termination */
                perror2("pthread_join",err); 
                exit(1); 
                }
        }
        
        for (i=0;i<NUMBER_OF_LOCKS;i++)
                pthread_mutex_destroy(&mutex_bloom[i]);

        printf("\n");
        if(found!=NULL){ //found is the global variable the threads check or use to keep the hidden word
                printf("FOUND THE WORD = (%s)\n",found);
        }
        else{
                printf("FAILED TO FIND THE WORD\n");
        }
        
        close(arg.file);
        free(log);
        free(addr);
        if(err=pthread_mutex_lock(&let_server_copy)!=0) {perror2("fatal error locking log file",err); exit;}
        free(bloom);
        free(thread_ids);
        free(found);
        free(mutex_bloom);
        printf("\n");
        return 0;
}

