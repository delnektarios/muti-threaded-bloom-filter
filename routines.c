
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h> 
#include <sys/types.h>     // For sockets 
#include <sys/socket.h>    // For sockets 
#include <netinet/in.h>    // For Internet sockets 
#include <netdb.h> 
#include <time.h>
#include "oracle.h"
#include "functions.h"
#include "routines.h"

static pthread_mutex_t log_mutex=PTHREAD_MUTEX_INITIALIZER;

ssize_t read_all(int fd, void *buf, uint64_t nbyte) // Function was found from  http://basepath.com/aup/ex/extio_8c-source.html
{
    ssize_t nread = 0, n;
    do {
        if ((n = read(fd, &((char *)buf)[nread], nbyte - nread)) == -1) {
            if (errno == EINTR)
                continue;
            else
                printf("Something went wrong  with  read  %s \n",strerror(errno));
                return -1;
        }
        if (n == 0)
         return nread;
        nread += n;
    } while (nread < nbyte);
    return nread;
}


ssize_t write_all (int fd, const void* buffer, uint64_t count)
{
  size_t left_to_write = count;
  while (left_to_write > 0) {
    size_t written = write (fd, buffer, count);
    if (written == -1)
      /* An error occurred; bail.  */
      return -1;
    else
      /* Keep count of how much more we need to write.  */
      left_to_write -= written;
  }
  /* We should have written no more than COUNT bytes!  */
  if(left_to_write != 0) printf("Problem with write.");
 
  /* The number of bytes written is exactly COUNT.  */
  return count;
}


//----------------------ROUTINE---------------------------------------------------
//routine function for thread
void *routine(void *arg) {
	int err,depth=0,i,j,seed,flag;
        char word[16],str[256]; 
	arguments *a=arg;
        memset(str,'\0',256);
        double c1,c2;

        for(i=0; i<a->L; i++){                  //looping as asked
                memset(word,'\0',16);           //initializing word
                seed=pthread_self()+i;          //initializing seed
                //creating a random word
                for(j=0;j<15;j++){          
                        word[j]=rand_r(&seed)%('Z' - 'A') + 'A';
                        if(word[j]=='\0') break;
                }
                //starting the recursion
                ask_oracle(a->N,a->bloom,a->num,word,&flag,a->mutex_bloom,a->NUMBER_OF_LOCKS,depth,&c1,&c2); //begin the recursion
                if(flag==1) pthread_exit(NULL); //IF THE HIDDEN WORD IS FOUND EXIT
        }
        c2=c2/c1;      
        //WRITTING INFORMATION INTO LOGFILE
        if(err=pthread_mutex_lock(&log_mutex)!=0) {perror2("fatal error locking log file",err); exit;}
        sprintf(str,"%ld",pthread_self());
        write(a->file,"I AM THREAD : ",14);
        write(a->file,str,strlen(str));
        write(a->file,"\n",1);
        write(a->file,"\n",1);

        memset(str,'\0',256);
        sprintf(str,"%d",c1);
        write(a->file,"TRIES : ",8);
        write(a->file,str,strlen(str));
        write(a->file,"\n",1);
        write(a->file,"\n",1);

        memset(str,'\0',256);
        sprintf(str,"%.2f",c2);
        write(a->file,"percentage : ",13);
        write(a->file,str,strlen(str));
        write(a->file,"\n",1);
        write(a->file,"\n",1);
        write(a->file,"\n",1);
        if(err=pthread_mutex_unlock(&log_mutex)!=0) {perror2("fatal error unlocking log file",err); exit;}
        pthread_exit(NULL); 
}

//----------------------SERVER ARGUMENT COPIER----------------------------------------------------
void *routine_to_copy(void *arg){

        arguments *a=arg;
        uint64_t tmp;
        int tmp1,i,send=0;
        
        //SERVER SENDS ALL ARGUMENTS TO CLIENT
        printf("socket to cominicate in routine = %d\n",a->socket);
        tmp=htonl(a->size);
        write_all(a->socket,&tmp,(uint64_t) sizeof(tmp));
        printf("ok\n");
                       	
        tmp1=htonl(a->num_thr);
        write_all(a->socket,&tmp1,(uint64_t) sizeof(a->num_thr));
        printf("ok\n");
                       	
        tmp1=htonl(a->L);
        write_all(a->socket,&tmp1,(uint64_t) sizeof(a->L));
        printf("ok\n");
                       	
        tmp1=htonl(a->port);
        write_all(a->socket,&tmp1,(uint64_t) sizeof(a->port));
        printf("ok\n");
                       	
        tmp1=htonl(a->num);
        write_all(a->socket,&tmp1,(uint64_t) sizeof(a->num));
        printf("ok\n");

        tmp1=htonl(a->seed);
        write_all(a->socket,&tmp1,(uint64_t) sizeof(a->num));
        printf("ok\n");
              
        //SERVER WAITS TO SEE IF CLIENT NEEDS THE BLOOM FILTER OR NOT         	
        read_all(a->socket,&send,(uint64_t) sizeof(send));
        fprintf(stderr,"send in server is %d\n",send);
        printf("ok\n");
        //SENDING BLOOM FILTER 					
        if(send==1){
                for (i=0;i<a->NUMBER_OF_LOCKS;i++) pthread_mutex_lock(&(a->mutex_bloom[i]));      
                fprintf(stderr,"all locks locked\n");	
                //UPLOADING BLOOM FILTER	
                write_all(a->socket,a->bloom,a->size);
                       		 
                for (i=0;i<a->NUMBER_OF_LOCKS;i++) pthread_mutex_unlock(&(a->mutex_bloom[i]));                     
                fprintf(stderr,"all locks unlocked\n");
        }
}


//---------------------SERVER ROUTINE (CODE FROM CLASS)--------------------------------------------
void *server_thread(void *arg){
        arguments *a=arg;
        uint64_t tmp;
        int  sock, newsock,length=0,tmp1,i,err; 
        char buf[8192];
        unsigned int serverlen, clientlen;
        struct sockaddr_in server, client;
        struct sockaddr *serverptr, *clientptr;
        struct hostent *rem;
        
        pthread_t copy_thread_id;

        printf("port number is = %d\n",a->port);
        if ((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0) { // Create socket 
                perror("socket"); 
                exit(1); 
        }
        server.sin_family = PF_INET;                      // Internet domain 
        server.sin_addr.s_addr = htonl(INADDR_ANY);   // My Internet address 
        server.sin_port = htons(a->port);                     // The given port 
        serverptr = (struct sockaddr *) &server;
        serverlen = sizeof server;

        if (bind(sock, serverptr, serverlen) < 0) {// Bind socket to address 
                perror("bind"); exit(1); }
        if (listen(sock, 5) < 0) {                 // Listen for connections 
                perror("listen"); exit(1); }
        printf("Listening for connections to port %d\n", a->port);
        while(1) {
                clientptr = (struct sockaddr *) &client;
                clientlen = sizeof client;
                if ((newsock = accept(sock, clientptr, &clientlen)) < 0) {
                        perror("accept"); exit(1); }              // Accept connection 
                printf("newsock is = %d\n",newsock);
                if ((rem = gethostbyaddr((char *) &client.sin_addr.s_addr,
                        sizeof client.sin_addr.s_addr,     // Find client's address 
                        client.sin_family)) == NULL) {
                        perror("gethostbyaddr"); 
                        exit(1); 
                }
                printf("Accepted connection from %s\n", rem -> h_name);

                a->socket=newsock;
                printf("socket to cominicate = %d\n",a->socket);

                if(err=pthread_mutex_lock(&let_server_copy)!=0) {perror2("fatal error locking server copy",err); exit;}
                if (err=pthread_create(&copy_thread_id,NULL,routine_to_copy,(void *) a)){
      	                /* Create a thread */
      	                perror2("pthread_create in server",err); 
      	                exit(12);
                } 
                if(err=pthread_join(copy_thread_id,NULL)){
                /* Wait for thread termination */
                perror2("pthread_join copy thread id",err); 
                exit(1); 
                }   
                if(err=pthread_mutex_unlock(&let_server_copy)!=0) {perror2("fatal error UNlocking server copy",err); exit;}
                close(newsock);// Close socket
        } 
}

