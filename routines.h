
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#ifndef ROUTINES //include guard

#define ROUTINES


ssize_t read_all(int fd, void *buf, uint64_t nbyte);

ssize_t write_all (int fd, const void* buffer, uint64_t count);

void *routine(void *arg);

void *routine_to_copy(void *arg);

void *server_thread(void *arg);

#endif
