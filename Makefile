

CC=gcc

CFLAGS=-c -w

LIBS=-L . -loracle_v3 -lhash -lm -lpthread 

all: execute
	#rm -rf *o invoke-oracle

#./invoke-oracle-multithreaded SIZE N L PORT LOGFILE [-k NUM] [-h ADDRESS]
execute: invoke-oracle-multithreaded
	./invoke-oracle-multithreaded 100000000 4 10 15345 log.txt 5 127.0.0.1 15345
        

invoke-oracle-multithreaded: oracle.o functions.o routines.o
	$(CC) oracle.o functions.o routines.o -o invoke-oracle-multithreaded $(LIBS)

oracle.o:
	$(CC) $(CFLAGS) oracle.c $(LIBS)

functions.o: 
	$(CC) $(CFLAGS) functions.c $(LIBS)

routines.o: 
	$(CC) $(CFLAGS) routines.c $(LIBS)


clean:
	rm -rf *o invoke-oracle-multithreaded
