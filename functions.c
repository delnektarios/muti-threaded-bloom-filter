
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <string.h>
#include <limits.h>
#include <pthread.h> 
#include <inttypes.h>
#include "hash.h"
#include "functions.h"

//MUTEX FOR GLOBAL VAR found
static pthread_mutex_t mymutex=PTHREAD_MUTEX_INITIALIZER;

//for http://www.algorithmist.com/index.php/Bubble_sort.c
void bubbleSort(int numbers[],int array_size){
        int i,j;
        int temp;
        for (i=(array_size-1); i > 0; i--){
                for (j =1;j<=i;j++){
                        if(numbers[j-1]>numbers[j]){
                        temp=numbers[j-1];
                        numbers[j-1] =numbers[j];
                        numbers[j]=temp;
                        }
                }
        }
}

// BLOOM FILTER
//---------------------------------------------------------------------------------------------------------------------------------
int bloom_filter(int N,uint64_t value[],int num,unsigned char *bloom,pthread_mutex_t *mutex_bloom,int NUMBER_OF_LOCKS){
        int position,check,exists=0,i,err;
        unsigned char val;
        int locks[num];

        for(i=0;i<num; i++){
                position=value[i]%N;
                locks[i]=position/(64*CHAR_BIT);
                //printf("%d\n",locks[i]);
        }

        bubbleSort(locks,num);
        //printf("PRINTING LOCKS[]\n");
        //for(i=0;i<num;i++) //printf("%d\n",locks[i]);
        for(i=0;i<num;i++){
                if(i>0 && locks[i]==locks[i-1]) continue;
                //fprintf(stderr,"trying to get lock %d\n",locks[i]);
                if(err=pthread_mutex_lock(&mutex_bloom[(locks[i])])!=0) {perror2("fatal error locking bloom",err); exit;}
                //printf("got lock %d\n",locks[i]);
        }
        for(i=0; i<num; i++){   //for the number of hash functions
                position=value[i]%N;    // (value) mod (number of bits in table) = position of the desired bit
		//val is used to have the desired bit in the position we want
		val=pow(2,position%CHAR_BIT);
                // use the operator & to check if the bit you need is 1
                check=val & bloom[position/CHAR_BIT];
                //use the operator | to change the wanted bit to 1
	        if(!check) bloom[position/CHAR_BIT]=val | bloom[position/CHAR_BIT];
                if(check) exists++; //if all the bits are 1,the word exists in bloom filter
        }
        for(i=0;i<num; i++){
                //fprintf(stderr,"trying to unlock %d\n",locks[i]);	
		//if(i>0 && locks[i]==locks[i-1]) continue;
                if(err=pthread_mutex_unlock(&mutex_bloom[(locks[i])])!=0) {perror2("fatal error unlocking bloom",err); exit;}
        }  
        if(exists==num) return 1; //if all the bits are 1,the word exists in bloom filter
        return 0;                 //otherwise,it does not exist
}
//---------------------------------------------------------------------------------------------------------------------------------
//this function runs the recursion
//-----------------------------------------------------------------------------------------------------------------------------------------
void ask_oracle(int N,char *bloom,int num,const char *word,int *flag,pthread_mutex_t *lock,int num_locks,int depth,double *c1,double *c2){
        //asking oracle a word
        int err;
        //fprintf(stderr,"in function with word = %s and number of locks = %d\n",word,num_locks);
        const char **table=oracle(word);
        //fprintf(stderr,"after asking %p\n",&(*table));
        //oracle answers
        if(table==NULL) { //WORD FOUND
                if(err=pthread_mutex_lock(&mymutex)!=0) { perror2("fatal error locking var",err); exit;}
                found=malloc(strlen(word)+1); 
                memset(found,'\0',strlen(word)+1);
                strcpy(found,word); //KEEPING THE HIDDEN WORD
                (*flag)=1;
                if(err=pthread_mutex_unlock(&mymutex)!=0) { perror2("fatal error unlocking var",err); exit;}
                return; 
        }
        
        if(table[0]==NULL) return; //no answers given from oracle

        depth++; //incrementing existing depth of recursion
        
        if((depth%20)==1){
                //fprintf(stderr,"trying to get lock mymutex\n");
                if(err=pthread_mutex_lock(&mymutex)!=0) {perror2("fatal error locking var",err); exit;}
                //fprintf(stderr,"got lock mymutex\n");
                if(found!=NULL){
                        if(err=pthread_mutex_unlock(&mymutex)!=0) {perror2("fatal error unlocking var",err); exit;}
                        (*flag)=1;
                        return;
                }   
                //fprintf(stderr,"trying to unlock mymutex\n"); 
                if(err=pthread_mutex_unlock(&mymutex)!=0) {perror2("fatal error unlocking var",err); exit;}      
        }            
        int check=0,i,t,j;
        uint64_t value[num];           
        // for each answer
        for(i=0; table[i]!=NULL; i++){
                for(t=0; t<num; t++) // calling the hush functions
                        value[t]=hash_by(t,table[i]);

                check=bloom_filter(N,value,num,bloom,lock,num_locks); //checking with bloom filter

                (*c1)++;
                if(check){ (*c2)++; continue; }//if the word exists in the filter, bypass it
                ask_oracle(N,bloom,num,table[i],flag,lock,num_locks,depth,c1,c2);
                if((*flag)==1) break;
        }
        for(i=0; table[i]!=NULL; i++)
                free(table[i]);
        free(table);
        return;
         
}
